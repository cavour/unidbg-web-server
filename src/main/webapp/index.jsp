<html>
<style>
    h1{
        height: 60px;
        color: #f35626;
        background: coral;
        background-image: -webkit-linear-gradient(45deg,#f35626,#feab3a);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        -webkit-animation: hue 6s infinite linear;
    }
    @-webkit-keyframes hue {
        from {
            -webkit-filter: hue-rotate(0deg);
        }
        to {
            -webkit-filter: hue-rotate(-360deg);
        }
    }
</style>
<body>
    <center><h1 >Hello World!</h1></center>
</body>
</html>
