package com.ts.unidbg.utils;

import com.alibaba.fastjson.JSON;
import com.ts.unidbg.vo.RequestApi;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 通过Request返回请求流中的Body数据以及其他方法
 *
 * @author MyPC, QuJianJun
 * @create 2018/4/11
 * Package_Name:com.ckflv.parse.utils
 */
public class RequestTools {
    /**
     * 传入请求流获取提交的body数据
     * @param request 请求流
     * @return  返回映射到的实体类
     */
    public static RequestApi getRequestBodyContent(HttpServletRequest request) {
        ServletInputStream is;
        String str = null;
        RequestApi requestApi;
        try {
            is = request.getInputStream();
            int nRead = 1;
            int nTotalRead = 0;
            byte[] bytes = new byte[10240];
            while (nRead > 0) {
                nRead = is.read(bytes, nTotalRead, bytes.length - nTotalRead);
                if (nRead > 0) {
                    nTotalRead = nTotalRead + nRead;
                }
            }
            str = new String(bytes, 0, nTotalRead, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        requestApi = JSON.parseObject(str,RequestApi.class);
        //如果提交过来的json数据为空的话，新创建一个对象,防止调用类做判断为空指针异常
        return requestApi==null?new RequestApi():requestApi;
    }
}
