package com.ts.unidbg.vo;

/**
 * 获取前端参数通用映射类
 *
 * @author MyPC, QuJianJun
 * @create 2018/4/11
 * Package_Name:com.ts.unidbg.vo
 */

public class RequestApi {

    /**
     * 登录用户名
     */
    private String userName;
    /**
     * 登录密码
     */
    private String passWord;
    /**
     * 参数key
     */
    private String param;
    /**
     * 京东参数1
     */
    private String jdParam1;
    /**
     * 京东参数2
     */
    private String jdParam2;
    /**
     * 京东参数3
     */
    private String jdParam3;
    /**
     * 京东参数4
     */
    private String jdParam4;
    /**
     * 京东参数5
     */
    private String jdParam5;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getJdParam1() {
        return jdParam1;
    }

    public void setJdParam1(String jdParam1) {
        this.jdParam1 = jdParam1;
    }

    public String getJdParam2() {
        return jdParam2;
    }

    public void setJdParam2(String jdParam2) {
        this.jdParam2 = jdParam2;
    }

    public String getJdParam3() {
        return jdParam3;
    }

    public void setJdParam3(String jdParam3) {
        this.jdParam3 = jdParam3;
    }

    public String getJdParam4() {
        return jdParam4;
    }

    public void setJdParam4(String jdParam4) {
        this.jdParam4 = jdParam4;
    }

    public String getJdParam5() {
        return jdParam5;
    }

    public void setJdParam5(String jdParam5) {
        this.jdParam5 = jdParam5;
    }
}
