package com.ts.unidbg.vo;


/**
 * Created by Administrator on 2018/3/26.
 *
 * @author QuJianJun
 */

public class ResponseApi {
    /**
     * 状态码
     */
    private int statusCode;
    /**
     * 提示信息
     */
    private String message;


    /**
     * 数据对象
     */

    private Object Data;

    private int total;
    public ResponseApi(int statusCode, String message, Object data) {
        this.statusCode = statusCode;
        this.message = message;
        Data = data;
    }

    public ResponseApi(int statusCode, String message, Object data, int total) {
        this.statusCode = statusCode;
        this.message = message;
        Data = data;
        this.total = total;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
