package com.ts.unidbg.service.impl;

import com.github.unidbg.AndroidEmulator;
import com.github.unidbg.linux.android.AndroidEmulatorBuilder;
import com.github.unidbg.linux.android.AndroidResolver;
import com.github.unidbg.linux.android.dvm.*;
import com.github.unidbg.linux.android.dvm.array.ArrayObject;
import com.github.unidbg.linux.android.dvm.array.ByteArray;
import com.github.unidbg.memory.Memory;
import com.ts.unidbg.service.JdService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.service.imp
 * @Description: 京东APP相关操作的实现类. APP版本 10.0.10
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-03 18:41:03
 */

@Service
public class JdServiceImpl extends AbstractJni implements JdService {

    private AndroidEmulator emulator = null;
    private VM vm = null;


    JdServiceImpl() {
        emulator = AndroidEmulatorBuilder.for32Bit().build(); // 创建模拟器实例，要模拟32位或者64位，在这里区分
        String filePath = Thread.currentThread().getContextClassLoader().getResource("jd/jd_10.0.10.apk").getPath();
        final Memory memory = emulator.getMemory(); // 模拟器的内存操作接口
        memory.setLibraryResolver(new AndroidResolver(23)); // 设置系统类库解析
        vm = emulator.createDalvikVM(new File(filePath));
        vm.setVerbose(true); // 设置是否打印Jni调用细节
        String bitmapSoPath = Thread.currentThread().getContextClassLoader().getResource("jd/libjdbitmapkit.so").getPath();
        DalvikModule dm = vm.loadLibrary(new File(bitmapSoPath), true);
        vm.setJni(this);
        dm.callJNI_OnLoad(emulator);
    }

    @Override
    public String getSign(String arg1, String arg2, String arg3, String arg4, String arg5) {
        DvmClass bitmapkitUtils = vm.resolveClass("com/jingdong/common/utils/BitmapkitUtils");
        DvmObject context = vm.resolveClass("android.content.Context").newObject(null);
        DvmObject<?> str = bitmapkitUtils.callStaticJniMethodObject(emulator, "getSignFromJni()(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
                context,
                vm.addLocalObject(new StringObject(vm,arg1)),
                vm.addLocalObject(new StringObject(vm,arg2)),
                vm.addLocalObject(new StringObject(vm,arg3)),
                vm.addLocalObject(new StringObject(vm,arg4)),
                vm.addLocalObject(new StringObject(vm,arg5))
                );
        return str.getValue().toString();
    }

    @Override
    public DvmObject<?> getStaticObjectField(BaseVM vm, DvmClass dvmClass, String signature) {
        System.out.println("getStaticObjectField--------------------------->" + signature);
        switch (signature) {
            case "com/jingdong/common/utils/BitmapkitUtils->a:Landroid/app/Application;":
                return vm.resolveClass("android/app/Application",
                        vm.resolveClass("android/content/ContextWrapper",
                                vm.resolveClass("android/app/Activity"))).newObject(signature);
            case "android/app/Application->sourceDir:Ljava/lang/String;":
                return vm.resolveClass("android/app/Application").newObject(signature);
        }
        return super.getStaticObjectField(vm, dvmClass, signature);
    }

    @Override
    public DvmObject<?> getObjectField(BaseVM vm, DvmObject<?> dvmObject, String signature) {
        System.out.println("getObjectField--------------------------->" + signature);
        switch (signature) {
            case "android/app/Application->sourceDir:Ljava/lang/String;":
                return vm.resolveClass("android/app/Application").newObject(signature);
        }
        return super.getObjectField(vm, dvmObject, signature);
    }

    @Override
    public DvmObject<?> callObjectMethod(BaseVM vm, DvmObject<?> dvmObject, String signature, VarArg varArg) {
        System.out.println("callObjectMethod---——----------->" + signature);
        switch (signature) {
            case "android/app/Application->getApplicationInfo()Landroid/content/pm/ApplicationInfo;":
                return vm.resolveClass("android/app/Application").newObject(signature);
            case "sun/security/pkcs/PKCS7->getCertificates()[Ljava/security/cert/X509Certificate;":
                DvmObject[] dvmObjects = {
                        vm.resolveClass("java/security/cert/X509Certificate").newObject(signature),
                };
                return new ArrayObject(dvmObjects);
        }
        return super.callObjectMethod(vm, dvmObject, signature, varArg);
    }

    @Override
    public DvmObject<?> callStaticObjectMethod(BaseVM vm, DvmClass dvmClass, String signature, VarArg varArg) {
        System.out.println("callStaticObjectMethod-------------------->" + signature);
        switch (signature) {
            case "com/jingdong/common/utils/BitmapkitZip->unZip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B":
                return new ByteArray(vm, "1234567890132456".getBytes(StandardCharsets.UTF_8));
            case "com/jingdong/common/utils/BitmapkitZip->objectToBytes(Ljava/lang/Object;)[B":
                byte[] result = hexStringToByteArray("aced00057372002d6a6176612e73656375726974792e636572742e436572746966696361746524436572746966696361746552657089276a9dc9ae3c0c0200025b0004646174617400025b424c0004747970657400124c6a6176612f6c616e672f537472696e673b7870757200025b42acf317f8060854e002000078700000024b30820247308201b0a00302010202044d6c5dae300d06092a864886f70d01010505003068310b300906035504061302636e3110300e060355040813076265696a696e67310e300c060355040713056368696e613111300f060355040a13086a696e67646f6e673111300f060355040b13086a696e67646f6e673111300f060355040313086a696e67646f6e67301e170d3131303330313032343530325a170d3338303731373032343530325a3068310b300906035504061302636e3110300e060355040813076265696a696e67310e300c060355040713056368696e613111300f060355040a13086a696e67646f6e673111300f060355040b13086a696e67646f6e673111300f060355040313086a696e67646f6e6730819f300d06092a864886f70d010101050003818d00308189028181008c470af7c751ee12edbae8dd9e7c98fa60d3c631efa0f7172ed36c86bb85c8288391e718c05fdbef008d61f2e8fce4ef4457a69ae5a2fa53ead0c806c18f8b475847c07bf4451d82845efc30d5fc4aa2500f4bc84234a36749e83a9361c9ec89771a762e3d791eebf3154c2e95d06df95be68b4a4dcff33ef1ba5d6d90758b6d0203010001300d06092a864886f70d010105050003818100821db5cf6e40d98e9b0e2a6a8ad09a0c52435e82df79d16fa8a3fea8f135f40cceedd3e98c18d0ef4bb5f5cfeccea3311d14e48cce863f520bc8b71491287fe5559d06a8a20446e168da5f488f971f473220ce619976e2286ce353b6882016d9978309edb3200a7aa0a5d5c8e39a7d9d612d9a6fc210d878f40d0860a59cd432740005582e353039");
                return new ByteArray(vm, result);
        }
        return super.callStaticObjectMethod(vm, dvmClass, signature, varArg);
    }

    @Override
    public DvmObject<?> newObject(BaseVM vm, DvmClass dvmClass, String signature, VarArg varArg) {
        System.out.println("newObject-------------------->" + signature);
        switch (signature) {
            case "sun/security/pkcs/PKCS7-><init>([B)V":
                return dvmClass.newObject(signature);
        }
        return super.newObject(vm, dvmClass, signature, varArg);
    }

    @Override
    public DvmObject<?> newObjectV(BaseVM vm, DvmClass dvmClass, String signature, VaList vaList) {
        System.out.println("newObjectV-------------------->" + signature);
        switch (signature){
            case "java/lang/StringBuffer-><init>()V":
                return vm.resolveClass("java/lang/StringBuffer").newObject(new StringBuffer());
            case "java/lang/Integer-><init>(I)V":
                return vm.resolveClass("java/lang/Integer").newObject(vaList.getIntArg(0));
        }
        return super.newObjectV(vm, dvmClass, signature, vaList);
    }

    @Override
    public DvmObject<?> callObjectMethodV(BaseVM vm, DvmObject<?> dvmObject, String signature, VaList vaList) {
        System.out.println("callObjectV-------------------->" + signature);
        switch (signature){
            case "java/lang/StringBuffer->append(Ljava/lang/String;)Ljava/lang/StringBuffer;":
                StringBuffer str = (StringBuffer) dvmObject.getValue();
                StringObject serviceName = vaList.getObjectArg(0);
                assert serviceName != null;
                return vm.resolveClass("java.lang.StringBuffer").newObject(str.append(serviceName.getValue()));
            case "java/lang/StringBuffer->toString()Ljava/lang/String;":
                StringBuffer strr = (StringBuffer) dvmObject.getValue();
                return new StringObject(vm,strr.toString());
            case "java/lang/Integer->toString()Ljava/lang/String;":
                return new StringObject(vm, String.valueOf(dvmObject.getValue()));
        }
        return super.callObjectMethodV(vm, dvmObject, signature, vaList);
    }

    /* s must be an even-length string. */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
