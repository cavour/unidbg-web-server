package com.ts.unidbg.service.impl;
/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.controller
 * @Description: 慢慢买 app的Token 生成服务接口功能实现类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021年08月10日22:03:40
 */
import com.github.unidbg.AndroidEmulator;
import com.github.unidbg.linux.android.AndroidEmulatorBuilder;
import com.github.unidbg.linux.android.AndroidResolver;
import com.github.unidbg.linux.android.dvm.*;
import com.github.unidbg.linux.android.dvm.array.ByteArray;
import com.github.unidbg.memory.Memory;
import com.ts.unidbg.service.MaiMaiBuy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

@Service
public class MaiMaiBuyImpl extends AbstractJni implements MaiMaiBuy {
    private AndroidEmulator emulator = null;
    private VM vm = null;

    /**
     * 获取生成参数的实现函数
     * @param param
     * @return
     */
    @Override
    public String getSign(String param) {
        DvmClass bitmapkitUtils = vm.resolveClass("com/maochunjie/mencryptsign/RNReactNativeMencryptSignModule");
        DvmObject<?> dvmObject = bitmapkitUtils.callStaticJniMethodObject(emulator, "getToken()(Ljava/lang/String;)Ljava/lang/String;",
                vm.addLocalObject(new StringObject(vm, param))
        );
        return dvmObject.getValue().toString();
    }

    MaiMaiBuyImpl() {
        emulator = AndroidEmulatorBuilder.for32Bit().build(); // 创建模拟器实例，要模拟32位或者64位，在这里区分
        final Memory memory = emulator.getMemory(); // 模拟器的内存操作接口
        memory.setLibraryResolver(new AndroidResolver(23)); // 设置系统类库解析
        try {
            vm = emulator.createDalvikVM(new ClassPathResource("maimaibuy/manmanbuy_3.6.90.apk").getFile());
            vm.setVerbose(true); // 设置是否打印Jni调用细节
            DalvikModule dm = vm.loadLibrary(new ClassPathResource("maimaibuy/libmmbKey.so").getFile(), true);
            vm.setJni(this);
            dm.callJNI_OnLoad(emulator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public DvmObject<?> newObjectV(BaseVM vm, DvmClass dvmClass, String signature, VaList vaList) {
        switch (signature) {
            case "java/lang/StringBuffer-><init>()V":
                return vm.resolveClass("java/lang/StringBuffer").newObject(new StringBuffer());
            case "java/lang/StringBuilder-><init>(I)V":
                return vm.resolveClass("java/lang/StringBuilder").newObject(new StringBuilder());
            case "java/io/ByteArrayInputStream-><init>([B)V":
                return vm.resolveClass("java/io/ByteArrayInputStream").newObject(vaList.getObjectArg(0).getValue());
        }
        return super.newObject(vm, dvmClass, signature, vaList);
    }


    @Override
    public DvmObject<?> newObjectV(BaseVM vm, DvmClass dvmClass, DvmMethod dvmMethod, VaList vaList) {
        System.out.println("newObjectV-------------------->");
        return super.newObjectV(vm, dvmClass, dvmMethod, vaList);
    }

    @Override
    public DvmObject<?> callStaticObjectMethodV(BaseVM vm, DvmClass dvmClass, String signature, VaList vaList) {
        System.out.println("---------maimaibuy--callStaticObjectMethodV-------------------->" + signature);
        switch (signature) {
            case "com/maochunjie/mencryptsign/MD5Util->getMD5String(Ljava/lang/String;)Ljava/lang/String;":
                StringObject str1 = vaList.getObjectArg(0);
                return new StringObject(vm, getMD5String(str1.getValue()));
            case "java/security/MessageDigest->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;":
                try {
                    MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
                    return vm.resolveClass("java/security/MessageDigest").newObject(messageDigest);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
        }
        return super.callStaticObjectMethodV(vm, dvmClass, signature, vaList);
    }

    @Override
    public DvmObject<?> callObjectMethodV(BaseVM vm, DvmObject<?> dvmObject, String signature, VaList vaList) {
        System.out.println("-------maimaibuy--callObjectMethodV-------------------->" + signature);
        switch (signature) {
            case "java/security/MessageDigest->digest([B)[B":
                MessageDigest messageDigest = (MessageDigest) dvmObject.getValue();
                return new ByteArray(vm, messageDigest.digest());
            case "java/security/cert/CertificateFactory->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;":
                CertificateFactory certificateFactory = null;
                try {
                    certificateFactory = CertificateFactory.getInstance("X.509");
                    ByteArrayInputStream is = new ByteArrayInputStream((byte[]) vaList.getObjectArg(0).getValue());
                    X509Certificate certificate = (X509Certificate)certificateFactory.generateCertificate(is);
                    return vm.resolveClass("java/security/cert/Certificate").newObject(certificate);
                } catch (CertificateException e) {
                    e.printStackTrace();
                }

                return null;
            case "java/lang/StringBuilder->append(Ljava/lang/String;)Ljava/lang/StringBuilder;":
                StringBuilder str = (StringBuilder) dvmObject.getValue();
                StringObject serviceName = vaList.getObjectArg(0);
                assert serviceName != null;
                System.out.println("----xxxxxxxxxxxxxx------" + serviceName.getValue());
                if (!serviceName.getValue().startsWith("C_AAID")) {
                    return vm.resolveClass("java.lang.StringBuilder").newObject(str.append("7F83010817815989101D7442BF760B3F"));
                }
                return vm.resolveClass("java.lang.StringBuilder").newObject(str.append(serviceName.getValue()));
            case "java/lang/StringBuilder->toString()Ljava/lang/String;":
                StringBuilder strr = (StringBuilder) dvmObject.getValue();
                String s = strr.toString();
                return new StringObject(vm, s);
        }
        return super.callObjectMethodV(vm, dvmObject, signature, vaList);
    }

    public static String getMD5String(String arg1) {
        MessageDigest v0;
        try {
            v0 = MessageDigest.getInstance("MD5");
        } catch (Exception v1) {
            v1.printStackTrace();
            return null;
        }
        v0.update(arg1.getBytes());
        return byteArray2HexString(v0.digest());
    }

    private static String byteArray2HexString(byte[] arg5) {
        StringBuilder v0 = new StringBuilder();
        int v2;
        for (v2 = 0; v2 < arg5.length; ++v2) {
            v0.append(String.format("%02X", Byte.valueOf(arg5[v2])));
        }
        return v0.toString();
    }

    public static void main(String[] args) {
        MaiMaiBuyImpl impl = new MaiMaiBuyImpl();
        String sign = impl.getSign("C_AAID7669727B-6D64-4CA4-992C-EA0569A5166CC_AB21578%2C21544C_APPVER3.6.90C_BRANDREDMIC_CHANNEL%E9%98%BF%E9%87%8C%E5%88%86%E5%8F%91%E5%B9%B3%E5%8F%B0C_CTRLTABSC_DEVID9C%3ABC%3AF0%3A07%3AE1%3ABEC_DEVMODELM2010J19SCC_DIDYYCO5OL35VHEFW32PNH3W63N7GVQZM4C5CLDRCUV44KNPBEK2AHA01C_DP1C_FIRSTCHANNEL%E9%98%BF%E9%87%8C%E5%88%86%E5%8F%91%E5%B9%B3%E5%8F%B0C_FIRSTQUERENDATE1628577465078C_FRISTVERSION3.6.90C_MAC9C%3ABC%3AF0%3A07%3AE1%3ABEC_MMBDEVIDC67DCC05645A445CAAF9A754869DDB51-597C_OAID74D3EEA28E169F23C_OSTYPEANDROIDC_OSVER10C_SAFEAREA28.363636016845703_0C_SSIDC35E8DE2-BFB7-412C-8E0E-D457BBE6388DC_TEST%5B%7B%22T%22%3A%22INDEXINFORMATIONFLOWTEST_3570%22%2C%22G%22%3A%22DEFAULT%22%7D%2C%7B%22T%22%3A%22TEST3610%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22INDEXFILTERBARABTEST3620%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22SEARCH_ZOUSHIQWBJABTEST3640%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22HISTORYTRENDZHEKOUDRAINAGEABTEST3640%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22ZHEKOUSEARCHBOXABTEST3680%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22SHEQUHOMEPAGEABTES3660%22%2C%22G%22%3A%22TESTA%22%7D%2C%7B%22T%22%3A%22APPINDEXNEWSTYLEABTEST3680%22%2C%22G%22%3A%22TESTB%22%7D%5DC_USERSTATUS%7B%22ST_TREND_USER_STATUS%22%3A%221%22%2C%22ST_SEARCH_ITEM_USER_STATUS%22%3A%221%22%2C%22ST_HOME_CX_LIST_STATUS%22%3A%221%22%2C%22ST_HOME_GUIDE_SUPERNATANT%22%3A%221%22%2C%22ST_SEARCH_HISTORY_USER_STATUS%22%3A%221%22%2C%22ST_SHEQU_RECOMMEND_ARITH%22%3A%221%22%2C%22ST_HOME_SERACH_BOX_STATUS%22%3A%221%22%2C%22ST_SEARCH_YH_MODAL%22%3A%221%22%7DC_VAID72300FA04A54CF19C_WINW_393_H_775JSONCALLBACK%3FMETHODNAMEGET_HOME_ALERTT1628601894606");
        System.out.println(sign);
    }
}
