package com.ts.unidbg.service.impl;

import com.github.unidbg.AndroidEmulator;
import com.github.unidbg.linux.android.AndroidEmulatorBuilder;
import com.github.unidbg.linux.android.AndroidResolver;
import com.github.unidbg.linux.android.dvm.*;
import com.github.unidbg.linux.android.dvm.array.ArrayObject;
import com.github.unidbg.linux.android.dvm.array.ByteArray;
import com.github.unidbg.memory.Memory;
import com.ts.unidbg.service.PddService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.service.impl
 * @Description: 拼多多接口实现类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-11 14:57:05
 */
@Service
public class PddServiceImpl extends AbstractJni implements PddService {
    private AndroidEmulator emulator = null;
    private VM vm = null;

    /**
     * 实现过程
     *
     * @param timestamp
     * @return
     */
    @Override
    public String getAntiToken(long timestamp) {
        DvmClass deviceNative = vm.resolveClass("com/xunmeng/pinduoduo/secure/DeviceNative");
        DvmObject context = vm.resolveClass("android/content/Context").newObject(null);
        DvmObject<?> dvmObject = deviceNative.callStaticJniMethodObject(emulator, "info2(Landroid/content/Context;J)Ljava/lang/String;",
                context,
                timestamp
        );
        return dvmObject.getValue().toString();
    }

    /**
     * 创建虚拟机 加载apk
     */
    PddServiceImpl() {
        emulator = AndroidEmulatorBuilder.for64Bit().build(); // 创建模拟器实例，要模拟32位或者64位，在这里区分
        final Memory memory = emulator.getMemory(); // 模拟器的内存操作接口
        memory.setLibraryResolver(new AndroidResolver(23)); // 设置系统类库解析
        try {
            vm = emulator.createDalvikVM(new ClassPathResource("pdd/pdd_5_7_40.apk").getFile());
            vm.setVerbose(true); // 设置是否打印Jni调用细节
            DalvikModule dm = vm.loadLibrary(new ClassPathResource("pdd/libpdd_secure.so").getFile(), true);
            vm.setJni(this);
            dm.callJNI_OnLoad(emulator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callStaticVoidMethodV(BaseVM vm, DvmClass dvmClass, String signature, VaList vaList) {
        System.out.println("------------callStaticVoidMethodV-------------->");
        switch (signature) {
            case "com/tencent/mars/xlog/PLog->i(Ljava/lang/String;Ljava/lang/String;)V":
                return;
        }
        super.callStaticVoidMethodV(vm, dvmClass, signature, vaList);
    }


    @Override
    public int callIntMethod(BaseVM vm, DvmObject<?> dvmObject, String signature, VarArg varArg) {
        System.out.println("------------callIntMethod-------------->" + signature);
        switch (signature) {
            case "android/content/Context->checkSelfPermission(Ljava/lang/String;)I":
                return -1;
            case "android/telephony/TelephonyManager->getSimState()I":
                return 1;
            case "android/telephony/TelephonyManager->getNetworkType()I":
                return 13;
            case "android/telephony/TelephonyManager->getDataState()I":
                return 0;
            case "android/telephony/TelephonyManager->getDataActivity()I":
                return 4;
        }
        return super.callIntMethod(vm, dvmObject, signature, varArg);
    }



    @Override
    public DvmObject<?> callObjectMethod(BaseVM vm, DvmObject<?> dvmObject, String signature, VarArg varArg) {
        System.out.println("------------callObjectMethod-------------->" + signature);
        switch (signature) {
            case "android/content/Context->getSystemService(Ljava/lang/String;)Ljava/lang/Object;":
                String arg = varArg.getObjectArg(0).getValue().toString();
                System.out.println("getSystemService arg:" + arg);
                return vm.resolveClass("android/telephony/TelephonyManager").newObject(signature);
            case "android/telephony/TelephonyManager->getSimOperatorName()Ljava/lang/String;":
                return new StringObject(vm, "中国联通");
            case "android/telephony/TelephonyManager->getSimCountryIso()Ljava/lang/String;":
                return new StringObject(vm, "cn");
            case "android/telephony/TelephonyManager->getNetworkOperatorName()Ljava/lang/String;":
                return new StringObject(vm, "中国联通");
            case "android/telephony/TelephonyManager->getNetworkOperator()Ljava/lang/String;":
                return new StringObject(vm, "46001");
            case "android/telephony/TelephonyManager->getNetworkCountryIso()Ljava/lang/String;":
                return new StringObject(vm, "cn");
            case "android/content/Context->getContentResolver()Landroid/content/ContentResolver;":
//                return vm.resolveClass("android/content/Context").newObject(signature);
                return null;
            case "java/lang/Throwable->getStackTrace()[Ljava/lang/StackTraceElement;":
                StackTraceElement[] elements = {
                        new StackTraceElement("com.xunmeng.pinduoduo.secure.DeviceNative","","",0),
                        new StackTraceElement("com.xunmeng.pinduoduo.secure.SecureNative","","",0),
                        new StackTraceElement("com.xunmeng.pinduoduo.secure.s","","",0),
                        new StackTraceElement("com.aimi.android.common.http.a","","",0),
                        new StackTraceElement("com.aimi.android.common.http.j","","",0),
                        new StackTraceElement("com.aimi.android.common.http.unity.internal.interceptor.k","","",0),
                        new StackTraceElement("okhttp3.internal.b.g","","",0),
                        new StackTraceElement("com.aimi.android.common.http.unity.internal.interceptor.PQuicInterceptor","","",0),
                        new StackTraceElement("okhttp3.internal.b.g","","",0),
                        new StackTraceElement("com.aimi.android.common.http.unity.internal.interceptor.g","","",0),
                        new StackTraceElement("okhttp3.internal.b.g","","",0),
                        new StackTraceElement("com.xunmeng.pinduoduo.arch.config.i$c","","",0),
                        new StackTraceElement("okhttp3.internal.b.g","","",0),
                        new StackTraceElement("com.xunmeng.pinduoduo.basekit.http.manager.b$4","","",0),
                };
                DvmObject[] objs = new DvmObject[elements.length];
                for (int i = 0; i < elements.length; i++) {
                    objs[i] =
                            vm.resolveClass("java/lang/StackTraceElement").newObject(elements[i]);
                }
                return new ArrayObject(objs);
            case "java/lang/StackTraceElement->getClassName()Ljava/lang/String;":
                StackTraceElement element = (StackTraceElement) dvmObject.getValue();
                return new StringObject(vm, element.getClassName());
            case "java/io/ByteArrayOutputStream->toByteArray()[B":
                ByteArrayOutputStream byteArrayOutputStream = (ByteArrayOutputStream) dvmObject.getValue();
                byte[] result = byteArrayOutputStream.toByteArray();
                return new ByteArray(vm, result);
        }
        return super.callObjectMethod(vm, dvmObject, signature, varArg);
    }


    @Override
    public DvmObject<?> getStaticObjectField(BaseVM vm, DvmClass dvmClass, String signature) {
        System.out.println("------------getStaticObjectField-------------->" + signature);
        switch (signature) {
            case "android/provider/Settings$Secure->ANDROID_ID:Ljava/lang/String;":
                return new StringObject(vm, "android_id");
        }
        return super.getStaticObjectField(vm, dvmClass, signature);
    }

    @Override
    public DvmObject<?> callStaticObjectMethodV(BaseVM vm, DvmClass dvmClass, String signature, VaList vaList) {
        System.out.println("------------callStaticObjectMethodV-------------->" + signature);
        switch (signature) {
            case "android/provider/Settings$Secure->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;":
                return new StringObject(vm, "");
            case "java/util/UUID->randomUUID()Ljava/util/UUID;":
                return vm.resolveClass("java/util/UUID").newObject(UUID.randomUUID());

        }
        return super.callStaticObjectMethodV(vm, dvmClass, signature, vaList);
    }

    @Override
    public boolean callStaticBooleanMethod(BaseVM vm, DvmClass dvmClass, String
            signature, VarArg varArg) {
        switch (signature){
            case "android/os/Debug->isDebuggerConnected()Z":{
                return false;
            }
        }
        return super.callStaticBooleanMethod(vm, dvmClass, signature, varArg);
    }

    @Override
    public DvmObject<?> newObject(BaseVM vm, DvmClass dvmClass, String signature, VarArg varArg) {
        switch (signature){
            case "java/lang/Throwable-><init>()V":
                return vm.resolveClass("java/lang/Throwable").newObject(new Throwable());
            case "java/io/ByteArrayOutputStream-><init>()V":
                return dvmClass.newObject(new ByteArrayOutputStream());
            case "java/util/zip/GZIPOutputStream-><init>(Ljava/io/OutputStream;)V":
                OutputStream outputStream = (OutputStream) varArg.getObjectArg(0).getValue();
                try {
                    return dvmClass.newObject(new GZIPOutputStream(outputStream));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        return super.newObject(vm, dvmClass, signature, varArg);
    }

    @Override
    public DvmObject<?> callObjectMethodV(BaseVM vm, DvmObject<?> dvmObject, String signature, VaList vaList) {
        switch (signature) {
            case "java/util/UUID->toString()Ljava/lang/String;":
                return new StringObject(vm, dvmObject.getValue().toString());
            case "java/lang/String->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;":
                StringObject str = (StringObject) dvmObject;
                StringObject s1 = vaList.getObjectArg(0);
                StringObject s2 = vaList.getObjectArg(1);
                assert s1 != null;
                assert s2 != null;
                return new StringObject(vm, str.getValue().replaceAll(s1.getValue(), s2.getValue()));
        }
        return super.callObjectMethodV(vm, dvmObject, signature, vaList);
    }

    @Override
    public void callVoidMethod(BaseVM vm, DvmObject<?> dvmObject, String signature, VarArg varArg) {
        switch (signature) {
            case "java/util/zip/GZIPOutputStream->write([B)V":
                GZIPOutputStream gzipOutputStream = (GZIPOutputStream) dvmObject.getValue();
                byte[] input = (byte[]) varArg.getObjectArg(0).getValue();
                try {
                    gzipOutputStream.write(input);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            case "java/util/zip/GZIPOutputStream->finish()V":
                GZIPOutputStream zipOutputStream = (GZIPOutputStream) dvmObject.getValue();
                try {
                    zipOutputStream.finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            case "java/util/zip/GZIPOutputStream->close()V":{
                GZIPOutputStream outputStream = (GZIPOutputStream) dvmObject.getValue();
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return; }
        }
        super.callVoidMethod(vm, dvmObject, signature, varArg);
    }
}
