package com.ts.unidbg.service;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.service
 * @Description: 京东APP相关操作的定义接口,APP版本 10.0.10
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-03 18:39:57
 */

public interface JdService {
        String getSign(String arg1, String arg2, String arg3, String arg4, String arg5);
}
