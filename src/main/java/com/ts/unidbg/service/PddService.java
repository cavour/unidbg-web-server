package com.ts.unidbg.service;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.service
 * @Description: 拼多多接口定义类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-11 14:55:35
 */

public interface PddService {

    /**
     * 拼多多获取Anti-Token接口定义类
     * @param timestamp
     * @return
     */
    String getAntiToken(long timestamp);
}
