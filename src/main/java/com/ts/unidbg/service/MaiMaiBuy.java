package com.ts.unidbg.service;
/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.controller
 * @Description: 慢慢买 app的Token 生成服务接口定义类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021年08月10日22:03:18
 */

public interface MaiMaiBuy {
    /**
     * 获取Token值 传入要加密的数据
     * @param param
     * @return
     */
    String getSign(String param);
}
