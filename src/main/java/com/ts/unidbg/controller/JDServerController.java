package com.ts.unidbg.controller;

import com.ts.unidbg.service.JdService;
import com.ts.unidbg.utils.RequestTools;
import com.ts.unidbg.vo.RequestApi;
import com.ts.unidbg.vo.ResponseApi;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.controller
 * @Description: 京东服务控制器
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-03 12:02:59
 */
@Controller()
@RequestMapping(value = "/jd")
public class JDServerController {

    @Autowired
    JdService jdService;


    @RequestMapping(value = "/getSig", method = RequestMethod.POST)
    @ResponseBody
    public ResponseApi getSign(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Content-Type", "application/json");
        RequestApi requestApi = RequestTools.getRequestBodyContent(request);
        String jdParam1 = requestApi.getJdParam1();
        String jdParam2 = requestApi.getJdParam2();
        String jdParam3 = requestApi.getJdParam3();
        String jdParam4 = requestApi.getJdParam4();
        String jdParam5 = requestApi.getJdParam5();
        String msg = "";
        int code = 200;
        if (StringUtils.isBlank(jdParam1)) {
            msg += "参数1[jdParam1]不允许为空.";
            code = 300;
        }
        if (StringUtils.isBlank(jdParam2)) {
            msg += "参数2[jdParam2]不允许为空.";
            code = 300;
        }
        if (StringUtils.isBlank(jdParam3)) {
            msg += "参数3[jdParam3]不允许为空.";
            code = 300;
        }
        if (StringUtils.isBlank(jdParam4)) {
            msg += "参数4[jdParam4]不允许为空.";
            code = 300;
        }
        if (StringUtils.isBlank(jdParam5)) {
            msg += "参数5[jdParam5]不允许为空.";
            code = 300;
        }
        if (code == 200) {
            msg = "操作成功~";
            synchronized(this) {
                String res = jdService.getSign(jdParam1, jdParam2, jdParam3, jdParam4, jdParam5);
                return new ResponseApi(code, msg, res);
            }
        }else{
            return new ResponseApi(code, msg, "");
        }
    }
}
