package com.ts.unidbg.controller;

import com.ts.unidbg.service.MaiMaiBuy;
import com.ts.unidbg.utils.RequestTools;
import com.ts.unidbg.vo.RequestApi;
import com.ts.unidbg.vo.ResponseApi;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.controller
 * @Description: 慢慢买 app的Token 服务
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-10 21:53:47
 */
@Controller()
@RequestMapping(value = "/maimaibuy")
public class MaimaiBuyController {


    @Autowired
    MaiMaiBuy maimaibuy;


    @RequestMapping(value = "/getSig", method = RequestMethod.POST)
    @ResponseBody
    public ResponseApi getSign(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Content-Type", "application/json");
        RequestApi requestApi = RequestTools.getRequestBodyContent(request);
        String param = requestApi.getParam();
        String msg = "";
        int code = 200;
        if (StringUtils.isBlank(param)) {
            msg += "参数[param]不允许为空.";
            code = 300;
        }
        if (code == 200) {
            msg = "操作成功~";
            synchronized(this) {
                String res = maimaibuy.getSign(param);
                return new ResponseApi(code, msg, res);
            }
        }else{
            return new ResponseApi(code, msg, "");
        }
    }
}
