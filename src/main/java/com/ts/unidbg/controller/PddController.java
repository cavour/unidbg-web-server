package com.ts.unidbg.controller;

import com.ts.unidbg.service.PddService;
import com.ts.unidbg.vo.ResponseApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @project: unidbgWebServer
 * @package: com.ts.unidbg.controller
 * @Description: 拼多多服务控制器
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-11 21:39:17
 */
@Controller
@RequestMapping(value = "/pdd")
public class PddController {


    @Autowired
    PddService pddService;

    /**
     * 获取拼多多anti-Token 控制器入口 api地址为/pdd/getToken
     * @param request
     * @param response
     * @return
     */

    @RequestMapping(value = "/getToken", method = RequestMethod.POST)
    @ResponseBody
    public ResponseApi getAntiToken(HttpServletRequest request, HttpServletResponse response){
        response.setHeader("Content-Type", "application/json");
        synchronized(this) {
            String res = pddService.getAntiToken(System.currentTimeMillis());
            return new ResponseApi(200,"操作成功",res);
        }
    }
}
