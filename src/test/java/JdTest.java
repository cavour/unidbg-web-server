import com.ts.unidbg.service.JdService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;

/**
 * @project: unidbgWebServer
 * @package: PACKAGE_NAME
 * @Description: 京东APP unidbg测试类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-03 18:43:26
 */

@RunWith(SpringJUnit4ClassRunner.class)		//表示继承了SpringJUnit4ClassRunner类
@ContextConfiguration(locations = {"classpath:spring-mybatis.xml", "classpath:spring-mvc.xml"}) //如果不操作数据库, 这里可以不要， 但是spring-mv.xml 必须要存在
public class JdTest {
    /**
     * 自动注入京东实现类对象
     */
    @Autowired
    JdService jdService;


    @Test
    public void loadFile() throws IOException {
        String filePath = Thread.currentThread().getContextClassLoader().getResource("jd/jd_10.0.10.apk").getPath();
        File f = new File(filePath);
        System.out.println(f.getName());
    }
}
