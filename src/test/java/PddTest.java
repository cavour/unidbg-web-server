import com.ts.unidbg.service.PddService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @project: unidbgWebServer
 * @package: PACKAGE_NAME
 * @Description: 拼多多测试类
 * @Since: 1.8.0_111
 * @Author: ts, QuJianJun
 * @Email: 8577352@qq.com
 * @Date: 2021-08-11 16:09:07
 */
@RunWith(SpringJUnit4ClassRunner.class)		//表示继承了SpringJUnit4ClassRunner类
@ContextConfiguration(locations = {"classpath:spring-mybatis.xml", "classpath:spring-mvc.xml"}) //如果不操作数据库, 这里可以不要， 但是spring-mv.xml 必须要存在
public class PddTest {

    @Autowired
    PddService pddService;

    @Test
    public void getAntiToken(){
        String res = pddService.getAntiToken(System.currentTimeMillis());
        System.out.println(res);
    }
}
